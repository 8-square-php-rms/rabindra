<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/main.css" media="screen" >
        
        <title>Document</title>
    </head>
    <body>
        <nav class="navbar top-navbar bg-info box-shadow">
            <div class="container">
                <div class="row">
                    <div class="navbar-header">
                        
                        <h1> STUDENT RESULT MANAGEMENT SYSTEM</h1>
                    </div>
                    
                    
                </div>
                
            </div>
            
        </nav>
        <div class="left-sidebar bg-black-300 box-shadow ">
            <div class="sidebar-content">
                
                <div class="sidebar-nav">
                    <ul class="side-nav color-gray">
                        
                        
                        <li class="has-children">
                            <a href="#"> <span>Student Classes</span> </a>
                            <ul class="child-nav">
                                <li><a href="class.php"> <span>Create Class</span></a></li>
                                <li><a href="manageclass.php"> <span>Manage Classes</span></a></li>
                                
                            </ul>
                        </li>
                        <li class="has-children">
                            <a href="#">Subjects</span></a>
                            <ul class="child-nav">
                                <li><a href="subjectcreate.php"> <span>Create Subject</span></a></li>
                                <li><a href="managesubject.php"> <span>Manage Subjects</span></a></li>
								<li><a href="subjectcombination.php"> <span>Subject Combination</span></a></li>
								<li><a href="managesubjectcomb.php"> <span>Manage Subject Combination</span></a></li>
								
                                
                            </ul>
                        </li>
                        <li class="has-children">
                            <a href="#"> <span>Students</span> </a>
                            <ul class="child-nav">
                                <li><a href="addstudent.php"> <span>Add Students</span></a></li>
                                <li><a href="managestudent.php"> <span>Manage Students</span></a></li>
                                
                            </ul>
                        </li>
                        <li class="has-children">
                            <a href="#"> <span>Result</span> </a>
                            <ul class="child-nav">
                                <li><a href="addresult.php"><span>Add Result</span></a></li>
                                <li><a href="manageresult.php"> <span>Manage Result</span></a></li>
								<li><a href="findresult.php"> <span>Find Result</span></a></li>
                                
                            </ul>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </body>
        </html>