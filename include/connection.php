<?php
        $servername= "localhost";
        $username= "root";
        $password= "";
        $dbname="MARKSHEET";
        $charset= "utf8";

        try
        {
            $db=new PDO("mysql:host={$servername};dbname={$dbname};charset={$charset}",$username,$password);
            
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo "Connection failed:".$e->getmessage();
        }
      
?>