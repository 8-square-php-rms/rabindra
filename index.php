<?php
include('connection.php');
if(isset($_POST['submit']))
{
$StudentName=$_POST['StudentName'];
$RollNo=$_POST['RollNo'];
$Email=$_POST['Email'];
$Gender=$_POST['Gender'];
$DOB=$_POST['DOB'];
$ClassId=$_POST['ClassId'];
$sql="INSERT INTO  student(StudentName,RollNo,Email,Gender,DOB,ClassId) VALUES(:StudentName,:RollNo,:Email,:Gender,:DOB,:ClassId)";
$query = $db->prepare($sql);
$query->bindParam(':StudentName',$StudentName,PDO::PARAM_STR);
$query->bindParam(':RollNo',$RollNo,PDO::PARAM_STR);
$query->bindParam(':Email',$Email,PDO::PARAM_STR);
$query->bindParam(':Gender',$Gender,PDO::PARAM_STR);
$query->bindParam(':ClassId',$ClassId,PDO::PARAM_STR);
$query->bindParam(':DOB',$DOB,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $db->lastInsertId();
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Student Registration< </title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="assets/css/main.css" media="screen" >
        
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            
            <div class="content-wrapper">
                <div class="content-container">
                    
                    <?php include('leftbar.php');?>
                    
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Student Registration</h2>
                                    
                                </div>
                                
                                
                            </div>
                            
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="#"> Home</a></li>
                                        
                                        <li class="active">Student Registration</li>
                                    </ul>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="container-fluid">
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <h5>Fill the Student info</h5>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <form class="form-horizontal" method="post">
                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Full Name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="StudentName" class="form-control" id="fullname" required="required" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Roll No</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="RollNo" class="form-control" id="rollid" maxlength="5" required="required" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <input type="email" name="Email" class="form-control" id="email" required="required" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Gender</label>
                                                    <div class="col-sm-10">
                                                        <input type="radio" name="Gender" value="Male" required="required" checked="">Male
                                                        <input type="radio" name="Gender" value="Female" required="required">Female
                                                        <input type="radio" name="Gender" value="Other" required="required">Other
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Class</label>
                                                    <div class="col-sm-10">
                                                        <select name="ClassId" class="form-control" id="default" required="required">
                                                            <option value="">Select Class</option>
                                                            <?php $sql = "SELECT * from classes";
                                                            $query = $db->prepare($sql);
                                                            $query->execute();
                                                            $results=$query->fetchAll(PDO::FETCH_OBJ);
                                                            if($query->rowCount() > 0)
                                                            {
                                                            foreach($results as $result)
                                                            {   ?>
                                                            <option value="<?php echo htmlentities($result->id); ?>"><?php echo htmlentities($result->ClassName); ?></option>
                                                            <?php }} ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="date" class="col-sm-2 control-label">DOB</label>
                                                    <div class="col-sm-10">
                                                        <input type="date"  name="DOB" class="form-control" id="date">
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" name="submit" class="btn btn-primary">Add</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
            
        </body>
    </html>