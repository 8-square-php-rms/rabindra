<?php
include ('connection.php');

if(isset($_POST['Update']))
{
$id=intval($_GET['id']);
$SubjectName=$_POST['SubjectName'];
$sql="UPDATE subjects SET SubjectName=:SubjectName where id=:id";
$query = $db->prepare($sql);
$query->bindParam(':SubjectName',$SubjectName,PDO::PARAM_STR);
$query->bindParam(':id',$id,PDO::PARAM_STR);
$query->execute();
$msg="Subject Info updated successfully";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Update Subject </title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" media="screen" >
        
        <link rel="stylesheet" href="assets/css/main.css" media="screen" >
        
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            
            <div class="content-wrapper">
                <div class="content-container">
                    
                    <?php include('leftbar.php');?>
                    
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Update Subject</h2>
                                    
                                </div>
                                
                                
                            </div>
                            
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Subjects</li>
                                        <li class="active">Update Subject</li>
                                    </ul>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="container-fluid">
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <h5>Update Subject</h5>
                                            </div>
                                        </div>
                                        
                                        <form class="form-horizontal" method="post">
                                            <?php
                                            $id=intval($_GET['id']);
                                            $sql = "SELECT * from subjects where id=:id";
                                            $query = $db->prepare($sql);
                                            $query->bindParam(':id',$id,PDO::PARAM_STR);
                                            $query->execute();
                                            $results=$query->fetchAll(PDO::FETCH_OBJ);
                                            $cnt=1;
                                            if($query->rowCount() > 0)
                                            {
                                            foreach($results as $result)
                                            {   ?>
                                            <div class="form-group">
                                                <label for="default" class="col-sm-2 control-label">Subject Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="SubjectName" value="<?php echo htmlentities($result->SubjectName);?>" class="form-control" id="default" placeholder="Subject Name" required="required">
                                                </div>
                                            </div>
                                            <?php }} ?>
                                            
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" name="Update" class="btn btn-primary">Update</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
        
    </body>
</html>