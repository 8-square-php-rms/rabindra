<?php
include('connection.php');

if(isset($_POST['submit']))
{
$SubjectName=$_POST['SubjectName'];
$sql="INSERT INTO subjects(SubjectName) VALUES(:SubjectName)";
$query = $db->prepare($sql);
$query->bindParam(':SubjectName',$SubjectName,PDO::PARAM_STR);
$query->execute();
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Subject Creation </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            
            <div class="content-wrapper">
                <div class="content-container">
                    
                    <?php include('leftbar.php');?>
                    >
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Subject Creation</h2>
                                    
                                </div>
                                
                                
                            </div>
                            
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"> Home</a></li>
                                        <li> Subjects</li>
                                        <li class="active">Create Subject</li>
                                    </ul>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="container-fluid">
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <h5>Create Subject</h5>
                                            </div>
                                        </div>

                                        <form class="form-horizontal" method="post">
                                            <div class="form-group">
                                                <label for="default" class="col-sm-2 control-label">Subject Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="SubjectName" class="form-control" id="default" placeholder="Subject Name" required="required">
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
    </body>
</html>