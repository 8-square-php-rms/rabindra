<?php
include ('connection.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Manage Subjects</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            
            <div class="content-wrapper">
                <div class="content-container">
                    <?php include('leftbar.php');?>
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Manage Subjects</h2>
                                    
                                </div>
                                
                                
                            </div>
                            
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="#">Home</a></li>
                                        <li> Subjects</li>
                                        <li class="active">Manage Subjects</li>
                                    </ul>
                                </div>
                                
                            </div>
                            
                        </div>
                        
                        <section class="section">
                            <div class="container-fluid">
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>View Subjects Info</h5>
                                                </div>
                                                
                                                <div class="panel-body p-20">
                                                    <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Subject Name</th>
                                                                
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        
                                                        <tbody>
                                                            <?php $sql = "SELECT * from subjects";
                                                            $query = $db->prepare($sql);
                                                            $query->execute();
                                                            $results=$query->fetchAll(PDO::FETCH_OBJ);
                                                            $cnt=1;
                                                            if($query->rowCount() > 0)
                                                            {
                                                            foreach($results as $result)
                                                            {   ?>
                                                            <tr>
                                                                <td><?php echo htmlentities($cnt);?></td>
                                                                <td><?php echo htmlentities($result->SubjectName);?></td>
                                                                
                                                                <td>
                                                                    <a href="editsubject.php?id=<?php echo htmlentities($result->id);?>">EDIT </a>
                                                                    <a href="deletesubject.php?id=<?php echo htmlentities($result->id);?>">DELETE </a>
                                                                </td>
                                                            </tr>
                                                            <?php $cnt=$cnt+1;}} ?>
                                                            
                                                            
                                                        </tbody>
                                                    </table>
                                                    
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </section>
            
        </div>
        
        
    </div>
    
</div>

</div>


</body>
</html>