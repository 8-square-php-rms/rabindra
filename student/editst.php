<?php
include('connection.php');
if($StudentId=$_GET['StudentId']){
if(isset($_POST['submit']))
{
$StudentName=$_POST['StudentName'];
$RollNo=$_POST['RollNo'];
$Email=$_POST['Email'];
$Gender=$_POST['Gender'];
$ClassId=$_POST['ClassName'];
$DOB=$_POST['DOB'];
$sql="UPDATE student SET StudentName=:StudentName,RollNo=:RollNo,Email=:Email,Gender=:Gender,DOB=:DOB, where StudentId=:StudentId ";
$query = $db->prepare($sql);
$query->bindParam(':StudentId',$StudentId,PDO::PARAM_STR);
$query->bindParam(':StudentName',$StudentName,PDO::PARAM_STR);
$query->bindParam(':RollNo',$RollNo,PDO::PARAM_STR);
$query->bindParam(':Email',$Email,PDO::PARAM_STR);
$query->bindParam(':Gender',$Gender,PDO::PARAM_STR);
$query->bindParam(':DOB',$DOB,PDO::PARAM_STR);
$query->execute();
$msg="Student info updated successfully";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SMS Admin| Edit Student < </title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="assets/css/main.css" media="screen" >
        
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            
            <div class="content-wrapper">
                <div class="content-container">
                    
                    <?php include('leftbar.php');?>
                    
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Student Admission</h2>
                                    
                                </div>
                                
                                
                            </div>
                            
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="#">Home</a></li>
                                        
                                        <li class="active">Student Admission</li>
                                    </ul>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="container-fluid">
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <h5>Fill the Student info</h5>
                                            </div>
                                            
                                            <form class="form-horizontal" method="post">
                                                <?php

                                                $sql = "SELECT student.StudentName,student.RollNo,student.StudentId,student.Email,student.Gender,student.DOB,classes.ClassName FROM student JOIN classes ON classes.id=student.ClassId WHERE student.StudentId=:StudentId";
                                                $query = $db->prepare($sql);
                                                $query->bindParam(':StudentId',$StudentId,PDO::PARAM_STR);
                                                $query->execute();
                                                $results=$query->fetchAll(PDO::FETCH_OBJ);
                                                $cnt=1;
                                                if($query->rowCount() > 0)
                                                {
                                                foreach($results as $result)
                                                {  ?>

                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Full Name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="StudentName" class="form-control" id="StudentName" value="<?php echo htmlentities($result->StudentName)?>" required="required" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Roll No</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="RollNo" class="form-control" id="RollNo" value="<?php echo htmlentities($result->RollNo)?>" maxlength="5" required="required" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Email </label>
                                                    <div class="col-sm-10">
                                                        <input type="email" name="Email" class="form-control" id="Email" value="<?php echo htmlentities($result->Email)?>" required="required" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Gender</label>
                                                    <div class="col-sm-10">
                                                        <?php  $gndr=$result->Gender;
                                                        if($gndr=="Male")
                                                        {
                                                        ?>
                                                        <input type="radio" name="Gender" value="Male" required="required" checked>Male <input type="radio" name="Gender" value="Female" required="required">Female <input type="radio" name="Gender" value="Other" required="required">Other
                                                        <?php }?>
                                                        <?php
                                                        if($gndr=="Female")
                                                        {
                                                        ?>
                                                        <input type="radio" name="Gender" value="Male" required="required" >Male <input type="radio" name="Gender" value="Female" required="required" checked>Female <input type="radio" name="Gender" value="Other" required="required">Other
                                                        <?php }?>
                                                        <?php
                                                        if($gndr=="Other")
                                                        {
                                                        ?>
                                                        <input type="radio" name="Gender" value="Male" required="required" >Male <input type="radio" name="Gender" value="Female" required="required">Female <input type="radio" name="Gender" value="Other" required="required" checked>Other
                                                        <?php }?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="default" class="col-sm-2 control-label">Class</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="ClassName" class="form-control" id="ClassName" value="<?php echo htmlentities($result->ClassName)?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="date" class="col-sm-2 control-label">DOB</label>
                                                    <div class="col-sm-10">
                                                        <input type="date"  name="DOB" class="form-control" value="<?php echo htmlentities($result->DOB)?>" id="date">
                                                    </div>
                                                </div>
                                                <?php }} ?>
                                                
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" name="submit" class="btn btn-primary">Add</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
            
        </body>
    </html>
    <?PHP } ?>