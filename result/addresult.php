<?php
include('connection.php');
if(isset($_POST['submit']))
{
$Marks=array();
$class=$_POST['class'];
$studentid=$_POST['studentid'];
$mark=$_POST['Marks'];
$stmt = $db->prepare("SELECT subjects.SubjectName,subjects.id FROM subjectcombination join  subjects on  subjects.id=subjectcombination.SubjectId WHERE subjectcombination.ClassId=:cid order by subjects.SubjectName");
$stmt->execute(array(':cid' => $class));
$sid1=array();
while($row=$stmt->fetch(PDO::FETCH_ASSOC))
{
array_push($sid1,$row['id']);
}

for($i=0;$i<count($mark);$i++){
$mar=$mark[$i];
$sid=$sid1[$i];
$sql="INSERT INTO  result(StudentId,ClassId,SubjectId,Marks) VALUES(:studentid,:class,:sid,:marks)";
$query = $db->prepare($sql);
$query->bindParam(':studentid',$studentid,PDO::PARAM_STR);
$query->bindParam(':class',$class,PDO::PARAM_STR);
$query->bindParam(':sid',$sid,PDO::PARAM_STR);
$query->bindParam(':marks',$mar,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $db->lastInsertId();
if($lastInsertId)
{
$msg="Result info added successfully";
}
else
{
$error="Something went wrong. Please try again";
}
}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Add Result </title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="assets/css/main.css" media="screen" >
        
        <script>
        function getStudent(val) {
        $.ajax({
        type: "POST",
        url: "getstude.php",
        data:'classid='+val,
        success: function(data){
        $("#studentid").html(data);
        
        }
        });
        $.ajax({
        type: "POST",
        url: "getstude.php",
        data:'classid1='+val,
        success: function(data){
        $("#subject").html(data);
        
        }
        });
        }
        </script>
        <script>
        function getresult(val,clid)
        {
        
        var clid=$(".clid").val();
        var val=$(".stid").val();;
        var abh=clid+'$'+val;
        //alert(abh);
        $.ajax({
        type: "POST",
        url: "getstude.php",
        data:'studclass='+abh,
        success: function(data){
        $("#reslt").html(data);
        
        }
        });
        }
        </script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            
            <div class="content-wrapper">
                <div class="content-container">
                    
                    <?php include('leftbar.php');?>
                    
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Declare Result</h2>
                                    
                                </div>
                                
                                
                            </div>
                            
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="#"> Home</a></li>
                                        <li class="active">Student Result</li>
                                    </ul>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="container-fluid">
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        
                                        
                                        <form class="form-horizontal" method="post">
                                            <div class="form-group">
                                                <label for="default" class="col-sm-2 control-label">Class</label>
                                                <div class="col-sm-10">
                                                    <select name="class" class="form-control clid" id="classid" onChange="getStudent(this.value);" required="required">
                                                        <option value="">Select Class</option>
                                                        <?php $sql = "SELECT * from classes";
                                                        $query = $db->prepare($sql);
                                                        $query->execute();
                                                        $results=$query->fetchAll(PDO::FETCH_OBJ);
                                                        if($query->rowCount() > 0)
                                                        {
                                                        foreach($results as $result)
                                                        {   ?>
                                                        <option value="<?php echo htmlentities($result->id); ?>"><?php echo htmlentities($result->ClassName); ?></option>
                                                        <?php }} ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="date" class="col-sm-2 control-label ">Student Name</label>
                                                <div class="col-sm-10">
                                                    <select name="studentid" class="form-control stid" id="studentid" required="required" onChange="getresult(this.value);">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                
                                                <div class="col-sm-10">
                                                    <div  id="reslt">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="date" class="col-sm-2 control-label">Subjects</label>
                                                <div class="col-sm-10">
                                                    <div  id="subject">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" name="submit" id="submit" class="btn btn-primary">Declare Result</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
        
        <script src="assets/js/jquery/jquery-2.2.4.min.js"></script>
        
        <script src="assets/js/main.js"></script>
        
    </body>
</html>