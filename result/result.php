<?php
session_start();
error_reporting(0);
include('connection.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Result Management System</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="assets/css/main.css" media="screen" >
        
    </head>
    <body>
        <div class="main-wrapper">
            <div class="content-wrapper">
                <div class="content-container">
                    
                    
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-12">
                                    <h2 class="title" align="center">Final Result </h2>
                                </div>
                            </div>
                            
                        </div>
                        
                        <section class="section">
                            <div class="container-fluid">
                                <div class="row">
                                    
                                    
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <?php
                                                    // code Student Data
                                                    $rollid=$_POST['rollid'];
                                                    $classid=$_POST['class'];
                                                    $_POST['rollid']=$rollid;
                                                    $_POST['classid']=$classid;
                                                    $qery = "SELECT   student.StudentName,student.RollNo,student.StudentId,classes.ClassName from student
                                                     join classes on classes.id=student.ClassId
                                                     where student.RollNo=:rollid and student.ClassId=:classid ";
                                                    $stmt = $db->prepare($qery);
                                                    $stmt->bindParam(':rollid',$rollid,PDO::PARAM_STR);
                                                    $stmt->bindParam(':classid',$classid,PDO::PARAM_STR);
                                                    $stmt->execute();
                                                    $resultss=$stmt->fetchAll(PDO::FETCH_OBJ);
                                                    $cnt=1;
                                                    if($stmt->rowCount() > 0)
                                                    {
                                                    foreach($resultss as $row)
                                                    {   ?>
                                                    <p><b>Student Name :</b> <?php echo htmlentities($row->StudentName);?></p>
                                                    <p><b>Student Roll No :</b> <?php echo htmlentities($row->RollNo);?>
                                                        <p><b>Student Class:</b> <?php echo htmlentities($row->ClassName);?>
                                                            <?php }
                                                            ?>
                                                        </div>
                                                        <div class="panel-body p-20">
                                                            <table class="table table-hover table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Subject</th>
                                                                        <th>Marks</th>
                                                                    </tr>
                                                                </thead>
                                                                
                                                                
                                                                <tbody>
                                                                    <?php
                                                                    // Code for result
                                                                    $query ="select t.StudentName,t.RollNo,t.ClassId,t.Marks,SubjectId,subjects.SubjectName from (select sts.StudentName,sts.RollNo,sts.ClassId,tr.Marks,SubjectId from student as sts join  result as tr on tr.StudentId=sts.StudentId) as t join subjects on subjects.id=t.SubjectId where (t.RollNo=:rollid and t.ClassId=:classid)";
                                                                    $query= $db -> prepare($query);
                                                                    $query->bindParam(':rollid',$rollid,PDO::PARAM_STR);
                                                                    $query->bindParam(':classid',$classid,PDO::PARAM_STR);
                                                                    $query-> execute();
                                                                    $results = $query -> fetchAll(PDO::FETCH_OBJ);
                                                                    $cnt=1;
                                                                    if($countrow=$query->rowCount()>0)
                                                                    {
                                                                    foreach($results as $result){
                                                                    ?>
                                                                    <tr>
                                                                        <th scope="row"><?php echo htmlentities($cnt);?></th>
                                                                        <td><?php echo htmlentities($result->SubjectName);?></td>
                                                                        <td><?php echo htmlentities($totalmarks=$result->Marks);?></td>
                                                                    </tr>
                                                                    <?php
                                                                    $totlcount=$totlcount+ $totalmarks;
                                                                    $cnt++;}
                                                                    ?>
                                                                    <tr>
                                                                        <th scope="row" colspan="2">Total Marks</th>
                                                                        <td><b><?php echo htmlentities($totlcount); ?></b> out of <b><?php echo htmlentities($outof=($cnt-1)*100); ?></b></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row" colspan="2">Percentage</th>
                                                                        <td><b><?php echo  number_format((float)($totlcount*(100)/$outof),2,'.',''); ?> %</b></td>
                                                                        
                                                                        <?php } else { ?>
                                                                        <div class="alert alert-warning left-icon-alert" role="alert">
                                                                            <strong>Notice!</strong> Your result not declare yet
                                                                            <?php }
                                                                            ?>
                                                                        </div>
                                                                        <?php
                                                                        } else
                                                                        {?>
                                                                        <div class="alert alert-danger left-icon-alert" role="alert">
                                                                            strong>Oh snap!</strong>
                                                                            <?php
                                                                            echo htmlentities("Invalid Roll Id");
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                                                   
                                                    <div class="form-group">
                                                        
                                                        <div class="col-sm-6">
                                                            <a href="addresult.php">Back to Home</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                            </div>
                                            
                                        </section>
                                        
                                    </div>
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                        
                        <script src="assets/js/jquery/jquery-2.2.4.min.js"></script>
                        <script src="assets/js/main.js"></script>
                        
     </body>
</html>