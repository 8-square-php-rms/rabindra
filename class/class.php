<?php
include('connection.php');

if(isset($_POST['submit']))
{
$classname=$_POST['classname'];
$sql="INSERT INTO classes(ClassName) VALUES(:classname)";
$query = $db->prepare($sql);
$query->bindParam(':classname',$classname,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $db->lastInsertId();

if($lastInsertId)
{
$msg="Class Created successfully";
}
else 
{
$error="Something went wrong. Please try again";
}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create Class</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="assets/css/main.css" media="screen">
</head>


<body class="top-navbar-fixed">


    <div class="main-wrapper"><br><br><br>
        <?php include ('leftbar.php');?>

        <div class="main-page">
            <div class="container-fluid">
                <div class="row page-title-div">
                    <div class="col-md-6">
                        <h2 class="title">Create Student Class</h2>
                    </div>
                </div>

                <div class="row breadcrumb-div">
                    <div class="col-md-6">
                        <ul class="breadcrumb">
                            <li><a href="#">Classes</a></li>
                            <li class="active">Create Class</li>
                        </ul>
                    </div>
                </div>
            </div>

            <section class="section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h5>Create Student Class</h5>
                                    </div>
                                </div>


                                <div class="panel-body">
                                    <form method="post">
                                        <div class="form-group has-success">
                                            <label for="success" class="control-label">Class Name</label>
                                            <div class="">
                                                <input type="text" name="classname" class="form-control" required="req id="
                                                    success">
                                                <span class="help-block">Eg- one, two,three etc</span>
                                            </div>
                                        </div>

                                        <div class="form-group has-success">
                                            <div class="">
                                                <button type="submit" name="submit" class="btn btn-success btn-labeled">Submit
                                                    <span class="btn-label btn-label-right"></span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>

</body>

</html>