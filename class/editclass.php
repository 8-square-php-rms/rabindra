<?php
include('connection.php');
if (isset($_POST['update']))
{
$id=$_POST['id'];
$ClassName= $_POST['ClassName'];

if($_POST['ClassName']!='')
{
try
{
$edit= $db->prepare("UPDATE classes SET ClassName= :ClassName WHERE id= :id");
$edit->bindParam(':id',$id);
$edit->bindParam(':ClassName',$ClassName);
$edit->execute();
echo"Inserted successfully";

}
catch (PDOException $e)
{
echo $e->getMessage();
}
}
else
{
$msg = "PLEASE FILL ALL FIELDS";
echo "<script type='text/javascript'>alert('$msg');</script>";
}

}
$id= 0;
$ClassName="";
if(isset($_GET['id'])){
$id=$_GET['id'];
$insert=$db->prepare('SELECT * FROM classes where id= :id');
$insert->execute(array(':id'=>$id));
$row=$insert->fetch();
$id=$row['id'];
$ClassName=$row['ClassName'];
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Update Class</title>
        <link rel="stylesheet" href="css/bootstrap.css" media="screen" >
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="assets/css/main.css" media="screen" >
        
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            
            <div class="content-wrapper">
                <div class="content-container">
                    <?php include('leftbar.php');?>
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Update Student Class</h2>
                                </div>
                                
                            </div>
                            
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="#.php">Home</a></li>
                                        <li><a href="#">Classes</a></li>
                                        <li class="active">Update Class</li>
                                    </ul>
                                </div>
                                
                            </div>
                            
                        </div>
                        
                        <section class="section">
                            <div class="container-fluid">
                                
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Update Student Class info</h5>
                                                </div>
                                                
                                            </div>
                                            
                                            <form method="post">
                                                <div class="form-group has-success">
                                                    <input type="hidden" name="id" value="<?=$id;?>">
                                                    <label for="success" class="control-label">Class Name</label>
                                                    <div class="">
                                                        <input type="text" name="ClassName" value="<?=$ClassName;?>" required="required" class="form-control" id="success">
                                                        <span class="help-block">Eg- Third, Fouth,Sixth etc</span>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group has-success">
                                                    <div class="">
                                                        <button type="submit" name="update" class="btn btn-success btn-labeled">Update<span class="btn-label btn-label-right"></span></button>
                                                    </div>
                                                    
                                                </form>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                
                                
                            </div>
                            
                        </section>
                        
                    </div>
                    
                    
                    
                </div>
                
            </div>
            
        </div>
        
    </body>
</html>