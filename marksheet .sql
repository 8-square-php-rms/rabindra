-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2019 at 10:46 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marksheet`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(100) NOT NULL,
  `ClassName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `ClassName`) VALUES
(24, 'one'),
(25, 'Two'),
(26, 'Three'),
(27, 'Four'),
(29, 'Five'),
(30, 'Six'),
(31, 'Seven'),
(33, 'Eight'),
(34, 'Nine'),
(35, 'Ten');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `StudentId` varchar(100) NOT NULL,
  `ClassId` varchar(100) NOT NULL,
  `SubjectId` varchar(100) NOT NULL,
  `Marks` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `StudentId`, `ClassId`, `SubjectId`, `Marks`) VALUES
(1, '3', '33', '10', 'Array'),
(2, '3', '33', '10', 'Array'),
(3, '3', '33', '5', 'Array'),
(4, '3', '33', '7', 'Array'),
(5, '3', '33', '6', 'Array'),
(6, '3', '33', '8', 'Array'),
(7, '3', '33', '9', 'Array'),
(8, '4', '33', '10', 'Array'),
(9, '4', '33', '5', 'Array'),
(10, '4', '33', '7', 'Array'),
(11, '4', '33', '6', 'Array'),
(12, '4', '33', '8', 'Array'),
(13, '4', '33', '9', 'Array'),
(14, '5', '34', '10', '94'),
(15, '6', '33', '10', '80'),
(16, '6', '33', '5', '76'),
(17, '6', '33', '7', '80'),
(18, '6', '33', '6', '60'),
(19, '6', '33', '8', '80'),
(20, '6', '33', '9', '67'),
(21, '7', '33', '10', '55'),
(22, '7', '33', '5', '67'),
(23, '7', '33', '7', '65'),
(24, '7', '33', '6', '56'),
(25, '7', '33', '8', '45'),
(26, '7', '33', '9', '56');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `StudentId` int(255) NOT NULL,
  `StudentName` varchar(100) NOT NULL,
  `RollNo` int(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `DOB` varchar(20) NOT NULL,
  `ClassId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`StudentId`, `StudentName`, `RollNo`, `Email`, `Gender`, `DOB`, `ClassId`) VALUES
(2, 'Rabindra Kunwar', 13, 'rabikunwar175@gmail.com', 'Male', '2012-12-31', '35'),
(3, 'Sushant Khatri', 15, 'suahant34@gmail.com', 'Male', '2018-10-29', '33'),
(4, 'Rakesh Thapa', 20, 'rakesh10@gmail.com', 'Male', '2008-08-26', '33'),
(5, 'SANTOSH KHATRI', 30, 'santosh@gmail.com', 'Male', '2008-10-04', '34'),
(6, 'Rabi Lamichhana', 17, 'rabi@gmail.com', 'Male', '1998-11-29', '33'),
(7, 'Hari Karki', 1, 'Haari45@gmail.com', 'Male', '2001-10-28', '33');

-- --------------------------------------------------------

--
-- Table structure for table `subjectcombination`
--

CREATE TABLE `subjectcombination` (
  `id` int(11) NOT NULL,
  `ClassId` varchar(100) NOT NULL,
  `SubjectId` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjectcombination`
--

INSERT INTO `subjectcombination` (`id`, `ClassId`, `SubjectId`) VALUES
(1, '33', '7'),
(2, '33', '5'),
(3, '33', '6'),
(4, '33', '8'),
(5, '33', '9'),
(6, '33', '10'),
(7, '34', '10');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `SubjectName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `SubjectName`) VALUES
(5, 'english'),
(6, 'Nepali'),
(7, 'Math'),
(8, 'Science'),
(9, 'Social'),
(10, 'Computer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `StudentId` (`StudentId`),
  ADD KEY `ClassId` (`ClassId`),
  ADD KEY `SubjectId` (`SubjectId`),
  ADD KEY `Marks` (`Marks`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`StudentId`),
  ADD KEY `ClassId` (`ClassId`),
  ADD KEY `ClassId_2` (`ClassId`);

--
-- Indexes for table `subjectcombination`
--
ALTER TABLE `subjectcombination`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ClassId` (`ClassId`),
  ADD KEY `SubjectId` (`SubjectId`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `StudentId` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subjectcombination`
--
ALTER TABLE `subjectcombination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
